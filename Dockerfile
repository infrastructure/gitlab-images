FROM registry.gitlab.com/gitlab-org/build/cng/gitlab-webservice-ce:v13.12.15 AS gitlab

FROM alpine AS patched
RUN apk add patch patchutils
COPY --from=gitlab /srv/gitlab /gitlab
COPY patches /patches
RUN cd /gitlab && for P in /patches/*.patch; do echo 🛠️  $P; filterdiff --exclude a/spec/* "$P" | patch -p1; done

FROM gitlab
COPY --from=patched /gitlab /srv/gitlab

